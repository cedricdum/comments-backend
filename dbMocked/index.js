import { config } from "dotenv";
import { MongoMemoryServer } from "mongodb-memory-server";
import mongoose from "mongoose";
import { readFileSync } from "fs";
import { join } from "path";

config();

const port = Number(process.env.DB_PORT);

// This will create an new instance of "MongoMemoryServer" and automatically start it
const mongod = await MongoMemoryServer.create({ instance: { port } });

const uri = mongod.getUri("commentsManager");
console.log("mongodb memory server uri:", uri);

(async () => {
  console.log("Populating...");
  const conn = await mongoose.connect(uri);

  console.log("Adding users...");
  const usersString = readFileSync(join("dbMocked", "users.json"));
  const users = JSON.parse(usersString);
  await mongoose.connection.collection("users").insertMany(users);

  console.log("Populate done !");
})();
