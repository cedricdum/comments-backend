# README #

## Prerequisites

- Node.js v14.18.1
- NPM 6.14.15

## Environment variables

It is mandatory to create your `.env` file, based on the `.env.template`.

## how to install

```
$ npm install
```

## How to start

### 1. Run the mocked database

Start a new terminal and launch the mocked database

```
$ npm run startDb
```

### 2. Run the backend

Start a new terminal and launch the backend

```
$ npm run start
```

## How to test

### 1. Stop the backend

### 2. Run the mocked database

Start a new terminal and launch the mocked database

```
$ npm run startDb
```
### 3. Run tests

```
$ npm test
```

# Issues

## Jest
Jest seems to work badly in a Node.js project with the ESM modules enabled by default.
When you try to `import` a module and spy a function, you got the following error

```
TypeError: Cannot assign to read only property 'checkToken' of object '[object Module]'
```

This is why the tests have not been completed.
Normally, it's possible to launch and initialize a mongoDB database in memory with the mongodb-memory-server library to run the unit tests
