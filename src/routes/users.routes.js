import express from "express";
import * as usersApi from "../business/users/index.js";
import checkAdmin from "./middleware/checkAdmin.js";

const router = express.Router();

router.post("/", checkAdmin, async (req, res, next) => {
  const { username, password } = req.body;
  try {
    const user = await usersApi.createOne(username, password);
    res.status(200).json(user);
  } catch (error) {
    res.status(500).json(error);
  }
});

router.get("/", checkAdmin, async (req, res, next) => {
  try {
    const users = await usersApi.getAll();
    res.status(200).json(users);
  } catch (error) {
    res.status(500).json(error);
  }
});

router.put("/:id", checkAdmin, async (req, res, next) => {
  try {
    const { id } = req.params;
    const user = req.body;
    const userUpdated = await usersApi.updateOne(id, user);
    res.status(200).json(userUpdated);
  } catch (error) {
    res.status(500).json(error);
  }
});

router.delete("/:id", checkAdmin, async (req, res, next) => {
  try {
    const { id } = req.params;
    const response = await usersApi.deleteOne(id);
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json(error);
  }
});

export default router;
