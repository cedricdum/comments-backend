import express from "express";
import cors from "cors";

import publicRoutes from "./public.routes.js";
import usersRoutes from "./users.routes.js";
import channelsRoutes from "./channels.routes.js";

const app = express();

const port = process.env.HTTP_PORT;

app.use(express.json());
app.use(cors());

app.use("/", publicRoutes);
app.use("/users", usersRoutes);
app.use("/channels", channelsRoutes);

app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`);
});

export default app;
