import jwt from "jsonwebtoken";
import { checkToken } from "../../business/authentication/index.js";

export default async function checkAdmin(req, res, next) {
  const sendUnauthenticate = (message) => res.status(401).json({ message });

  const { token } = req.headers;

  if (!token) {
    return sendUnauthenticate("Missing token");
  }

  if (!checkToken(token)) {
    return sendUnauthenticate("Unknown token");
  }

  try {
    const decoded = jwt.decode(token);
    if (decoded.role !== "admin") {
      return sendUnauthenticate("You do not have the admin role");
    }
    next();
  } catch (error) {
    console.error("Error in checkAdmin middleware", error);
    return sendUnauthenticate("Unknown error");
  }
}
