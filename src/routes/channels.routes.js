import express from "express";
import * as channelsApi from "../business/channels/index.js";
import checkAdmin from "./middleware/checkAdmin.js";

const router = express.Router();

router.get("/", async (req, res, next) => {
  try {
    const channels = await channelsApi.getAll();
    res.status(200).json(channels);
  } catch (error) {
    res.status(500).json(error);
  }
});

router.get("/getOne", async (req, res, next) => {
  const { userId, orderId, coordinates } = req.body;
  try {
    const channels = await channelsApi.getOne({ userId, orderId, coordinates });
    res.status(200).json(channels);
  } catch (error) {
    res.status(500).json(error);
  }
});

router.put("/updateOne", async (req, res, next) => {
  const { userId, orderId, coordinates, comments } = req.body;
  try {
    const channel = await channelsApi.updateOne({
      userId,
      orderId,
      coordinates,
      comments,
    });
    res.status(200).json(channel);
  } catch (error) {
    res.status(500).json(error);
  }
});

router.delete("/:id", checkAdmin, async (req, res, next) => {
  try {
    const { id } = req.params;
    const response = await channelsApi.deleteOne(id);
    res.status(200).json(response);
  } catch (error) {
    res.status(500).json(error);
  }
});

export default router;
