import connectDb from "./models/index.js";
import app from "./routes/index.js";
import wss from "./websocket/index.js";

connectDb();
