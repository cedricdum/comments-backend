import mongoose from "mongoose";

const port = Number(process.env.DB_PORT);
const url = `mongodb://localhost:${port}/commentsManager`;

mongoose.set("returnOriginal", false);
let mongooseConnectDb;

export default function connectDb() {
  if (!mongooseConnectDb) {
    mongooseConnectDb = mongoose.connect(url);
  }

  return mongooseConnectDb;
}

// Models
export { default as user } from "./user.js";
export { default as channel } from "./channel.js";
