import mongoose from "mongoose";

const { Schema } = mongoose;

const channelSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  orderId: {
    type: String,
  },
  georeferenceId: {
    type: {
      type: String, // Don't do `{ location: { type: String } }`
      enum: ["Point"], // 'location.type' must be 'Point'
      required: true,
    },
    coordinates: {
      type: [Number],
      required: true,
    },
  },
  comments: {
    type: [String],
  },
});

channelSchema.static("getAll", function () {
  return this.find();
});

channelSchema.static("getOne", function ({ userId, orderId, coordinates }) {
  let georeferenceId;
  if (coordinates) {
    georeferenceId = {
      type: "Point",
      coordinates,
    };
  }

  return this.find({ userId, orderId, georeferenceId });
});

channelSchema.static(
  "updateOne",
  function ({ userId, orderId, coordinates, comments }) {
    let georeferenceId;
    if (coordinates) {
      georeferenceId = {
        type: "Point",
        coordinates,
      };
    }

    return this.findOneAndUpdate(
      { userId, orderId, georeferenceId },
      { $push: { comments } },
      { upsert: true }
    );
  }
);

channelSchema.static("deleteOneById", function (_id) {
  return this.deleteOne({ _id });
});

const Channel = mongoose.model("Channel", channelSchema);

export default Channel;
