import mongoose from "mongoose";

const { Schema } = mongoose;

const userSchema = new Schema(
  {
    username: {
      type: String,
      unique: true,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    role: String,
  },
  {
    toJSON: {
      transform: function (doc, ret) {
        delete ret.password;
      },
    },
  }
);

userSchema.static("createOne", function (username, password) {
  return this.create({ username, password, role: "user" });
});

userSchema.static("getAll", function () {
  return this.find();
});

userSchema.static("updateOne", function (_id, user) {
  return this.findOneAndUpdate({ _id }, { $set: user });
});

userSchema.static("deleteOneById", function (_id) {
  return this.deleteOne({ _id });
});

userSchema.static("authenticate", function (username, password) {
  return this.findOne({ username, password });
});

const User = mongoose.model("User", userSchema);

export default User;
