import connectDb, * as models from "../../models/index.js";

export async function createOne(username, password) {
  await connectDb();
  try {
    return models.user.createOne(username, password);
  } catch (error) {
    console.error("Error while adding a new user:", error);
    throw error;
  }
}

export async function getAll() {
  await connectDb();
  try {
    return models.user.getAll();
  } catch (error) {
    console.error("Error while getting all user:", error);
    throw error;
  }
}

export async function updateOne(_id, user) {
  await connectDb();
  try {
    return models.user.updateOne(_id, user);
  } catch (error) {
    console.error("Error while updating an user:", error);
    throw error;
  }
}

export async function deleteOne(_id) {
  await connectDb();
  try {
    return models.user.deleteOneById(_id);
  } catch (error) {
    console.error("Error while deleting an user:", error);
    throw error;
  }
}
