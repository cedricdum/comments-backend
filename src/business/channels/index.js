import connectDb, * as models from "../../models/index.js";
import io from "../../websocket/index.js";

export async function getAll() {
  await connectDb();
  try {
    return models.channel.getAll();
  } catch (error) {
    console.error("Error while getting all channel:", error);
    throw error;
  }
}

export async function getOne({ userId, orderId, coordinates }) {
  if (!userId || (!orderId && !coordinates)) {
    throw new TypeError(
      "Parameters error: userId is mandatory and you need to provide orderId and/or coordinates"
    );
  }

  await connectDb();
  try {
    return models.channel.getOne();
  } catch (error) {
    console.error("Error while getting one channel:", error);
    throw error;
  }
}

export async function updateOne({ userId, orderId, coordinates, comments }) {
  if (!userId || (!orderId && !coordinates)) {
    throw new TypeError(
      "Parameters error: userId is mandatory and you need to provide orderId and/or coordinates"
    );
  }

  await connectDb();
  let channel;
  try {
    channel = await models.channel.updateOne({
      userId,
      orderId,
      coordinates,
      comments,
    });
  } catch (error) {
    console.error("Error while updating a channel:", error);
    throw error;
  }

  io.emit("updateChannel", channel);
  return channel;
}

export async function deleteOne(id) {
  await connectDb();
  try {
    return models.channel.deleteOneById(id);
  } catch (error) {
    console.error("Error while deleting a channel:", error);
    throw error;
  }
}
