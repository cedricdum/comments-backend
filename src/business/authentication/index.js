import connectDb, * as models from "../../models/index.js";
import AuthenticationError from "./authenticationError.js";
import jwt from "jsonwebtoken";

export const tokens = new Map();
const secret = "mySecret";

export async function login(username, password) {
  await connectDb();
  let user;
  try {
    user = await models.user.authenticate(username, password);
  } catch (error) {
    console.error("Error while authenticating an user");
    throw error;
  }

  if (!user) {
    throw new AuthenticationError(1, "bad credentials");
  }

  const token = jwt.sign({ username, role: user.role }, secret);
  tokens.set(username, token);
  return { token, userId: user._id, role: user.role };
}

export function checkToken(token) {
  for (const [key, value] of tokens.entries()) {
    if (value === token) return true;
  }
  return false;
}
