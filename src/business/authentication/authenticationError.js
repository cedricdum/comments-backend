export default class AuthenticationError extends Error {
  constructor(code, ...args) {
    // Passer les arguments restants au constructeur parent
    super(...args);

    // Maintenir dans la pile une trace adéquate de l'endroit où l'erreur a été déclenchée (disponible seulement en V8)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, AuthenticationError);
    }

    this.name = "AuthenticationError";
    // Informations de déboguage personnalisées
    this.code = code;
    this.date = new Date();
  }

  toJSON() {
    return {
      name: this.name,
      message: this.message,
      code: this.code,
      date: this.date,
      stack: this.stack,
    };
  }
}
