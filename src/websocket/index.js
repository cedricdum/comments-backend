import { Server } from "socket.io";

const io = new Server(process.env.WS_PORT, { path: "/" });

io.on("connection", (socket) => {
  console.log("new socket connexion");
});

export default io;
