import { expect, jest, test } from "@jest/globals";
import * as authenticateApi from "../../../src/business/authentication/index.js";
import AuthenticationError from "../../../src/business/authentication/AuthenticationError.js";
import * as models from "../../../src/models/index.js";

const username = "username";
const password = "password";
const role = "admin";

describe("Login", () => {
  test("authenticate throws an error", async function () {
    const error = new Error("Error from mongoose");
    jest
      .spyOn(models.user, "authenticate")
      .mockImplementationOnce(() => Promise.reject(error));

    await expect(authenticateApi.login()).rejects.toEqual(error);
  });

  test("no user found", async function () {
    jest
      .spyOn(models.user, "authenticate")
      .mockImplementationOnce(() => Promise.resolve(null));

    try {
      await authenticateApi.login();
    } catch (error) {
      expect(error.code).toEqual(1);
      expect(error).toBeInstanceOf(AuthenticationError);
    }
  });

  test("found user", async function () {
    jest
      .spyOn(models.user, "authenticate")
      .mockImplementationOnce(() =>
        Promise.resolve({ username, password, role })
      );

    await expect(authenticateApi.login()).resolves.toHaveProperty("token");
  });
});

describe("Check token", () => {
  test("token not exists", async function () {
    expect(authenticateApi.checkToken("foo")).toBeFalsy;
  });

  test("token not exists", async function () {
    const token = "gee";
    authenticateApi.tokens.set("bar", token);
    expect(authenticateApi.checkToken(token)).toBeTruthy;
  });
});
