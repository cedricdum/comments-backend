import { expect, jest, test } from "@jest/globals";
import * as usersApi from "../../../src/business/users/index.js";
import * as models from "../../../src/models/index.js";

const username = "username";
const password = "password";
const role = "admin";

describe("Create one user", function () {
  test("database throws an error", async function () {
    const error = new Error("Error from mongoose");
    jest
      .spyOn(models.user, "createOne")
      .mockImplementationOnce(() => Promise.reject(error));

    await expect(usersApi.createOne()).rejects.toEqual(error);
  });

});

describe("Get all users", function () {
  test("database throws an error", async function () {
    const error = new Error("Error from mongoose");
    jest
      .spyOn(models.user, "getAll")
      .mockImplementationOnce(() => Promise.reject(error));

    await expect(usersApi.getAll()).rejects.toEqual(error);
  });
});

describe("Update one user", function () {
  test("database throws an error", async function () {
    const error = new Error("Error from mongoose");
    jest
      .spyOn(models.user, "updateOne")
      .mockImplementationOnce(() => Promise.reject(error));

    await expect(usersApi.updateOne()).rejects.toEqual(error);
  });
});

describe("Delete one user", function () {
  test("database throws an error", async function () {
    const error = new Error("Error from mongoose");
    jest
      .spyOn(models.user, "deleteOne")
      .mockImplementationOnce(() => Promise.reject(error));

    await expect(usersApi.deleteOne()).rejects.toEqual(error);
  });
});
