import { expect, jest, test } from "@jest/globals";
import * as channelsApi from "../../../src/business/channels/index.js";
import * as models from "../../../src/models/index.js";

const userId = "userId";
const orderId = "orderId";
const georeferenceId = [1.1, 2.2];

describe("Create one channel", function () {
  test("call function without parameters", async function () {
    await expect(channelsApi.updateOne()).rejects.toThrow(TypeError);
  });

  test("database throws an error", async function () {
    const error = new Error("Error from mongoose");
    jest
      .spyOn(models.channel, "updateOne")
      .mockImplementationOnce(() => Promise.reject(error));

    await expect(
      channelsApi.updateOne({ userId, orderId, georeferenceId })
    ).rejects.toEqual(error);
  });
});

describe("Get all channels", function () {
  test("database throws an error", async function () {
    const error = new Error("Error from mongoose");
    jest
      .spyOn(models.channel, "getAll")
      .mockImplementationOnce(() => Promise.reject(error));

    await expect(channelsApi.getAll()).rejects.toEqual(error);
  });
});

describe("Get one channel", function () {
  test("call function without parameters", async function () {
    await expect(channelsApi.getOne()).rejects.toThrow(TypeError);
  });

  test("database throws an error", async function () {
    const error = new Error("Error from mongoose");
    jest
      .spyOn(models.channel, "getOne")
      .mockImplementationOnce(() => Promise.reject(error));

    await expect(
      channelsApi.getOne({ userId, orderId, georeferenceId })
    ).rejects.toEqual(error);
  });
});

describe("Update one channel", function () {
  test("database throws an error", async function () {
    const error = new Error("Error from mongoose");
    jest
      .spyOn(models.channel, "updateOne")
      .mockImplementationOnce(() => Promise.reject(error));

    await expect(
      channelsApi.updateOne({ userId, orderId, georeferenceId })
    ).rejects.toEqual(error);
  });
});

describe("Delete one channel", function () {
  test("database throws an error", async function () {
    const error = new Error("Error from mongoose");
    jest
      .spyOn(models.channel, "deleteOne")
      .mockImplementationOnce(() => Promise.reject(error));

    await expect(channelsApi.deleteOne()).rejects.toEqual(error);
  });
});
