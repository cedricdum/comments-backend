import { expect, test } from "@jest/globals";
import connectDb from "../../src/models/index.js";

describe("Mongoose connection", () => {
  test("Get connection object", function () {
    const promise = connectDb();
    expect(promise).toBeInstanceOf(Promise);
    expect(promise).resolves.toBeDefined();
  });
});
