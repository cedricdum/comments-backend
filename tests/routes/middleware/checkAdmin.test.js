import { afterAll, afterEach, expect, jest, test } from "@jest/globals";
import jwt from "jsonwebtoken";
import checkAdmin from "../../../src/routes/middleware/checkAdmin.js";
import * as authenticateApi from "../../../src/business/authentication/index.js";

const secret = "mySecret";
const username = "username";
const role = "role";
const token = jwt.sign({ username, role }, secret);

describe("checkAdmin middleware", () => {
  let req = { headers: { token } };
  let res = {};
  let next;

  beforeAll(() => {
    res.status = jest.fn(() => res);
    res.json = jest.fn(() => res);
    next = jest.fn(() => {});
  });

  afterEach(() => {
    next.mockReset();
    res.status.mockReset();
    res.json.mockReset();
  });

  afterAll(() => {
    next.mockRestore();
    res.status.mockRestore();
    res.json.mockRestore();
  });

  test("request without token returns 401", async function () {
    const req = { headers: {} };
    await checkAdmin(req, res, next);

    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledWith(401);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: "Missing token" });
    expect(next).not.toHaveBeenCalled();
  });

  test("request with a token not emitted by the system returns 401", async function () {
    jest
      .spyOn(authenticateApi, "checkToken")
      .mockImplementationOnce(() => false);

    await checkAdmin(req, res, next);

    expect(res.status).toHaveBeenCalledTimes(1);
    expect(res.status).toHaveBeenCalledWith(401);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: "Unknown token" });
    expect(next).not.toHaveBeenCalled();
  });
});
